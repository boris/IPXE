#!/bin/bash

# GET ISO FILES

# LMDE-LIVE-64 - OK
cd /tmp
url="https://ftp.crifo.org/mint-cd/debian/lmde-5-cinnamon-64bit.iso"
wget $url
sudo mount -o loop lmde-5-cinnamon-64bit.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/lmde-live-64
sudo umount /mnt
sudo rm -rf /tmp/lmde-5-cinnamon-64bit.iso

# SHREDOS-64
url="https://github.com/PartialVolume/shredos.x86_64/releases/download/v2021.08.2_21_x86-64_0.32.023/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso"
wget $url
sudo mount -o loop ./shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/shredos-64
sudo umount /mnt
sudo rm -rf /tmp/shredos-2021.08.2_21_x86-64_0.32.023_20220126.iso

# DEBIAN-LIVE-64 - OK
url="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-11.6.0-amd64-gnome.iso"
wget $url
sudo mount -o loop ./debian-live-11.6.0-amd64-gnome.iso /mnt
sudo cp -r /mnt/. /var/lib/tftpboot/debian-live-64/
sudo umount /mnt
sudo rm -rf /tmp/debian-live-11.6.0-amd64-gnome.iso
sudo mv /var/lib/tftpboot/debian-live-64/live/vmlinuz* /var/lib/tftpboot/debian-live-64/live/vmlinuz
sudo mv /var/lib/tftpboot/debian-live-64/live/initrd* /var/lib/tftpboot/debian-live-64/live/initrd.img